package org.pago;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class APIMethods {
    public static Response get(String endpoint) {
        return RestAssured.get(Endpoints.BASE_URL + endpoint);
    }

    public static Response post(String endpoint, String requestBody) {
        return RestAssured.given().body(requestBody).post(Endpoints.BASE_URL + endpoint);
    }

    public static Response deleteClientById(String clientId) {
        String endpoint = Endpoints.CLIENTS + "/" + clientId;
        return RestAssured.delete(Endpoints.BASE_URL + endpoint);
    }
    public static Response getClientById(String clientId) {
        String endpoint = Endpoints.CLIENTS + "/" + clientId;
        return RestAssured.get(Endpoints.BASE_URL + endpoint);
    }

}