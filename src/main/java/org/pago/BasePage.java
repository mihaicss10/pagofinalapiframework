package org.pago;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import static org.testng.Assert.assertEquals;

public class BasePage {
    public BasePage() {
        RestAssured.baseURI = Endpoints.BASE_URL;
    }

    public String getASpecificClientIdFromTheList(int clientIdPosition) {
        Response response = APIMethods.get(Endpoints.CLIENTS);
        String id = null;
        assertEquals(response.getStatusCode(), 200, "GET Clients endpoint failed: Expected status code 200");
        // Parse the JSON response
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            // Parse the response body as JSON
            JsonNode responseBody = objectMapper.readTree(response.getBody().asString());

            // Assuming the response is an array of objects, get the first object
            JsonNode firstObject = responseBody.get(0);

            // Extract the "id" field from the first object
            id = firstObject.get("id").asText();

            // Now you can save the "id" value for further use
            System.out.println("ID from the first object: " + id);

            // Add more assertions or actions as needed
        } catch (Exception e) {
            // Handle any exceptions that may occur during JSON parsing
            e.printStackTrace();
        }
        return id;
    }
}