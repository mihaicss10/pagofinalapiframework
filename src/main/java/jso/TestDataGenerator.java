package jso;
import com.github.javafaker.Faker;


public class TestDataGenerator {
    private static final Faker faker = new Faker();

    public static ClientData generateRandomClientData() {
        ClientData clientData = new ClientData();
        clientData.setName(faker.name().firstName());
        clientData.setLocation(faker.address().city());
        clientData.setId(String.valueOf(faker.number().randomNumber()));
        return clientData;
    }
}