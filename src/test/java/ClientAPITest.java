import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import jso.ClientData;
import jso.TestDataGenerator;
import org.pago.APIMethods;
import org.pago.BasePage;
import org.pago.Endpoints;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ClientAPITest extends BasePage {

    @Test
    public void testGetClientsEndpoint() {
        Response response = APIMethods.get(Endpoints.CLIENTS);

        assertEquals(response.getStatusCode(), 200, "GET Clients endpoint failed: Expected status code 200");
    }

    @Test
    public void testPostClientEndpoint() {
        String requestBody = "{ \"name\": \"name 111\", \"location\": \"location 100\", \"id\": \"44\" }";
        Response response = APIMethods.post(Endpoints.CLIENTS, requestBody);

        // Assert status code
        assertEquals(response.getStatusCode(), 201, "POST Client endpoint failed: Expected status code 201");
    }

    @Test
    public void testGetClientByIdEndpoint() {
        String clientId = getASpecificClientIdFromTheList(0);
        Response response = APIMethods.getClientById(clientId);

        // Assert different status codes
        assertEquals(response.getStatusCode(), 200, "GET Client by ID endpoint failed: Expected status code 200");
        // Add more assertions for other status codes
    }

    @Test
    public void testDeleteClientByIdEndpoint() {
        String id = getASpecificClientIdFromTheList(0);
        Response responseForDeletedItem = APIMethods.deleteClientById(id);

        // Assert  status codes
        assertEquals(responseForDeletedItem.getStatusCode(), 200, "DELETE Client by ID endpoint failed: Expected status code 200");
        // send a GET request to verify that the client no longer exists
        Response getResponse = APIMethods.getClientById(id);
        assertEquals(getResponse.getStatusCode(), 500, "GET Client by ID endpoint failed: Expected status code 500");
    }

    //This test will fail, because you cannot use this endpoint to create your own values
    @Test
    public void testPostClientEndpointWithRandomData() throws JsonProcessingException {
        ClientData randomClientData = TestDataGenerator.generateRandomClientData();
        String requestBody = new ObjectMapper().writeValueAsString(randomClientData);
        Response response = APIMethods.post(Endpoints.CLIENTS, requestBody);
        assertEquals(response.getStatusCode(), 201, "POST Client endpoint failed: Expected status code 201");

        // Extract the response body as a JSON string
        String responseBody = response.getBody().asString();

        // Deserialize the response JSON into a ClientData object
        ClientData responseClientData = new ObjectMapper().readValue(responseBody, ClientData.class);

        // Assert individual fields in the response
        assertEquals(responseClientData.getName(), randomClientData.getName(), "Name mismatch in response");
        assertEquals(responseClientData.getLocation(), randomClientData.getLocation(), "Location mismatch in response");
        assertEquals(responseClientData.getId(), randomClientData.getId(), "ID mismatch in response");
    }

    @Test
    public void testCreateAndDeleteClient() throws JsonProcessingException {
        // Step 1: Create a new client by sending a POST request
        ClientData randomClientData = TestDataGenerator.generateRandomClientData();
        String requestBody = new ObjectMapper().writeValueAsString(randomClientData);

        Response response = APIMethods.post(Endpoints.CLIENTS, requestBody);

        assertEquals(response.getStatusCode(), 201, "POST Client endpoint failed: Expected status code 201");

        // Extract the response body as a JSON string
        String responseBody = response.getBody().asString();

        // Deserialize the response JSON into a ClientData object
        ClientData responseClientData = new ObjectMapper().readValue(responseBody, ClientData.class);

        // Step 2: Extract the ID of the newly created client from the response
        String clientId = responseClientData.getId();

        // Step 3: Send a DELETE request to delete the client by ID
        Response deleteResponse = APIMethods.deleteClientById(clientId);

        // Step 4: Assert the response status code for successful deletion
        assertEquals(deleteResponse.getStatusCode(), 200, "DELETE Client endpoint failed: Expected status code 204");

        // send a GET request to verify that the client no longer exists
        Response getResponse = APIMethods.get(clientId);
        assertEquals(getResponse.getStatusCode(), 404, "GET Client by ID endpoint failed: Expected status code 404");
    }
}